https://about.gitlab.com/2017/02/07/setting-up-gitlab-pages-with-cloudflare-certificates/

Used letsencrypt successfully but requires updating certificates in gitlab by hand.
https://docs.gitlab.com/ee/user/project/pages/getting_started_part_three.html
https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt/
https://certbot.eff.org/docs/using.html#manual
